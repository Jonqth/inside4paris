//
//  InsideSplitViewController.h
//  inside4paris
//
//  Created by Jonathan Araujo-Levy on 24/11/12.
//  Copyright (c) 2012 hetic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"

@interface InsideSplitViewController : UIViewController


// Methods
- (UIViewController *) pushFromMenuView:(NSString *)className;

@end
