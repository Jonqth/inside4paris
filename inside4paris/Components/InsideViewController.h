//
//  InsideViewController.h
//  inside4paris
//
//  Created by Jonathan Araujo-Levy on 15/11/12.
//  Copyright (c) 2012 hetic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "IIViewDeckController.h"
#import "SplitViewController.h"

@class IIViewDeckController;
@class SplitViewController;

@interface InsideViewController : UIViewController

// Vars
@property (assign, nonatomic) BOOL isFromMenu;
@property (strong, nonatomic) NSString *placeDataType;

// Navigation elements
@property (strong,nonatomic) UINavigationBar *mainNavigationBar;
@property (strong,nonatomic) UINavigationItem *mainNavigationItem;

// Views
@property (strong, nonatomic) IIViewDeckController *insideDeckViewController;
@property (strong, nonatomic) SplitViewController *splitViewController;

// Methods
- (id)initWithDataType:(NSString *)dataType;
- (void) SetTextualTitleView:(NSString *)textAsTitle;
- (void) presentCurrentView:(UIViewController *)currentViewController;
- (void) pushToNextView:(UIViewController *)toController;
- (void) setMenuButton;
- (void) setExitButton;
- (void) setBackButton;
- (void) switchToPreviousView;
- (void) hideNavigationBar;
- (void) showNavigationBar;

@end
