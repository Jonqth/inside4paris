//
//  InsideViewController.m
//  inside4paris
//
//  Created by Jonathan Araujo-Levy on 15/11/12.
//  Copyright (c) 2012 hetic. All rights reserved.
//

#import "InsideViewController.h"
#import "IIViewDeckController.h"

@interface InsideViewController ()

@end

@implementation InsideViewController

// Navigation elements
@synthesize mainNavigationBar;
@synthesize mainNavigationItem;

- (id)initWithDataType:(NSString *)dataType {
    
    self.placeDataType = dataType;
    
    // Provenence
    self.isFromMenu = NO;
    
    self = [super init];
    if (self) {}
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        // AppDelegate
        [sharedAppDelegate.insideNavigationController setNavigationBarHidden:YES];
        
        [self.view setBackgroundColor:[UIColor colorWithRed:(237/255.0f) green:(236/255.0f) blue:(233/255.0f) alpha:1]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNavigationBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// **
// Navigation methods
// @superclass InsideViewController
// **

// Push to next view

- (void) pushToNextView:(UIViewController *)toController {
    [self.navigationController pushViewController:toController animated:YES];
}

// Send to the previous view

- (void) switchToPreviousView {
    [self.navigationController popViewControllerAnimated:YES];
}

// Present current ModalView

- (void) presentCurrentView:(UIViewController *)currentViewController {
    [self presentViewController:currentViewController animated:YES completion:nil];
}

// Dismiss current ModalView

- (void) dismissCurrentView {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// **
// Series of elements
// @superclass InsideViewController
// **

// Set the main NavigationBar

- (void) setNavigationBar {
    self.mainNavigationBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [self.mainNavigationBar setBarStyle:UIBarMetricsDefault];
    [self.mainNavigationBar setTintColor:[UIColor clearColor]];
    [self.mainNavigationBar setBackgroundImage:[UIImage imageNamed:@"i4p_navBarBg.jpg"] forBarMetrics:UIBarMetricsDefault];
    
    UIImageView *logoNavView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"i4p_logoNavBar.png"]];
    self.mainNavigationItem = [[UINavigationItem alloc] init];
    [self.mainNavigationItem setTitleView:logoNavView];
    
    [self.mainNavigationBar setItems:[NSArray arrayWithObject:self.mainNavigationItem] animated:NO];
    [self.view addSubview:self.mainNavigationBar];
}

// Set the menu button on a second level view

- (void) setMenuButton {
    UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *menuButtonButtonImageNormal = [UIImage imageNamed:@"i4p_menuButtonNormal.png"];
    UIImage *menuButtonButtonImageHighlighted = [UIImage imageNamed:@"i4p_menuButtonHighlighted.png"];
    [menuButton setFrame:CGRectMake(0, 0, menuButtonButtonImageNormal.size.width, menuButtonButtonImageNormal.size.height)];
    [menuButton setBackgroundImage:menuButtonButtonImageNormal forState:UIControlStateNormal];
    [menuButton setBackgroundImage:menuButtonButtonImageHighlighted forState:UIControlStateHighlighted];
    [menuButton addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    [self.mainNavigationBar addSubview:menuButton];
}

// Set the back button on a second level view

- (void) setBackButton {
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *backButtonImageNormal = [UIImage imageNamed:@"i4p_backButtonNormal.png"];
    UIImage *backButtonImageHighlighted = [UIImage imageNamed:@"i4p_backButtonHighlighted.png"];
    [backButton setFrame:CGRectMake(0, 0, backButtonImageNormal.size.width, backButtonImageNormal.size.height)];
    [backButton setBackgroundImage:backButtonImageNormal forState:UIControlStateNormal];
    [backButton setBackgroundImage:backButtonImageHighlighted forState:UIControlStateHighlighted];
    [backButton addTarget:self action:@selector(switchToPreviousView) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *ItemBackButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [self.mainNavigationItem setLeftBarButtonItem:ItemBackButton];
    [self.mainNavigationBar setItems:[NSArray arrayWithObject:self.mainNavigationItem] animated:NO];
}

// Set the exit button on a second level view

- (void) setExitButton {
    UIButton *navInfoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *navInfoButtonImageNormal = [UIImage imageNamed:@"i4p_exitButtonNormal.png"];
    UIImage *navInfoButtonImageHighlighted = [UIImage imageNamed:@"i4p_exitButtonHighlighted.png"];
    [navInfoButton setFrame:CGRectMake(0, 0, navInfoButtonImageNormal.size.width, navInfoButtonImageNormal.size.height)];
    [navInfoButton setBackgroundImage:navInfoButtonImageNormal forState:UIControlStateNormal];
    [navInfoButton setBackgroundImage:navInfoButtonImageHighlighted forState:UIControlStateHighlighted];
    [navInfoButton addTarget:self action:@selector(dismissCurrentView) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *infoButton = [[UIBarButtonItem alloc] initWithCustomView:navInfoButton];
    [self.mainNavigationItem setLeftBarButtonItem:infoButton];
    [self.mainNavigationBar setItems:[NSArray arrayWithObject:self.mainNavigationItem] animated:NO];
}

// Set the second level view title view

- (void) SetTextualTitleView:(NSString *)textAsTitle {
    UILabel * titleView = [[UILabel alloc] initWithFrame:CGRectZero];
    [titleView setBackgroundColor:[UIColor clearColor]];
    [titleView setFont:sharedAppDelegate.insideTitleFont];
    [titleView setShadowColor:[UIColor colorWithWhite:1.0 alpha:0.5]];
    [titleView setShadowOffset:CGSizeMake(0.0f, 1.0f)];
    [titleView setTextColor:sharedAppDelegate.insideGreyColor];
    [titleView setText:textAsTitle];
    [self.mainNavigationItem setTitleView:titleView];
    [titleView sizeToFit];
}

// Set the segmented view for subNavigationViews

- (void) setSegmentedView {
    
}

// hide the current NavigationBar

- (void) hideNavigationBar {
    [self.mainNavigationBar setHidden:YES];
}

// Show the current NavigationBar

- (void) showNavigationBar {
    [self.mainNavigationBar setHidden:NO];
}

@end
