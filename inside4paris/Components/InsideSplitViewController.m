//
//  InsideSplitViewController.m
//  inside4paris
//
//  Created by Jonathan Araujo-Levy on 24/11/12.
//  Copyright (c) 2012 hetic. All rights reserved.
//

#import "InsideSplitViewController.h"

// Views Imports
#import "PlacesListviewController.h"
#import "ItinerariesListViewController.h"
#import "EventListViewController.h"
#import "GoldenBookViewController.h"
#import "LegalMentionsViewController.h"
#import "CreditsViewController.h"
#import "MainViewController.h"


@interface InsideSplitViewController (){
    UIViewController *PushMenuController;
}

@end

@implementation InsideSplitViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {}
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

// Push to wanted view controller from menu

- (UIViewController *) pushFromMenuView:(NSString *)className {
    
    if([className isEqualToString:@"Lieux"]){
        PushMenuController = [[PlacesListviewController alloc] init];
        return PushMenuController;
    }else if ([className isEqualToString:@"Accueil"]){
        PushMenuController = [[MainViewController alloc] init];
        return PushMenuController;
    }else if ([className isEqualToString:@"Itinéraires"]){
        PushMenuController = [[ItinerariesListViewController alloc] init];
        return PushMenuController;
    }else if ([className isEqualToString:@"Evénements"]){
        PushMenuController = [[EventListViewController alloc] init];
        return PushMenuController;
    }else if ([className isEqualToString:@"Livre d'or"]){
        PushMenuController = [[GoldenBookViewController alloc] init];
        return PushMenuController;
    }else if ([className isEqualToString:@"Mentions légales"]){
        PushMenuController = [[LegalMentionsViewController alloc] init];
        return PushMenuController;
    }else if ([className isEqualToString:@"Crédits"]){
        PushMenuController = [[CreditsViewController alloc] init];
        return PushMenuController;
    }
    else{ return nil; }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
