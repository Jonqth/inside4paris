//
//  insideAppDelegate.m
//  inside4paris
//
//  Created by Jonathan Araujo-Levy on 18/10/12.
//  Copyright (c) 2012 hetic. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"
#import "LoginViewController.h"
#import "IIViewDeckController.h"
#import "SplitViewController.h"
#import "RestKit/RestKit.h"

@implementation AppDelegate


// Core Data elements
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

// Navigation elements
@synthesize insideNavigationController;

// Views
@synthesize insideDeckViewController;
@synthesize splitViewController;

// Vars
@synthesize mainScreenI5;
@synthesize isLogged;
@synthesize insideMainFont;
@synthesize insideTitleFont;
@synthesize insideBoldFont;
@synthesize insidePinkColor;
@synthesize insideGreyColor;
@synthesize insideTinyFont;

- (id)init {
    isLogged = NO;
    
    // Screen iPhone 5
    if([[UIScreen mainScreen]bounds].size.height == 568){
        self.mainScreenI5 = YES;
    }
    else{
        self.mainScreenI5 = NO;
    }
    
    // Main Font Settings
    insideMainFont = [UIFont fontWithName:@"Helvetica Neue" size:13.5f];
    insideTitleFont = [UIFont fontWithName:@"Helvetica Neue" size:14.5f];
    insideBoldFont = [UIFont fontWithName:@"Helvetica Neue-Bold" size:14.5f];
    insideTinyFont = [UIFont fontWithName:@"Helvetica Neue" size:10.5f];
    
    // Main Color Settings
    insidePinkColor = [UIColor colorWithRed:(183/255.f) green:(13/255.f) blue:(99/255.f) alpha:1];
    insideGreyColor = [UIColor colorWithRed:(71/255.0f) green:(71/255.0f) blue:(71/255.0f) alpha:1];
    
    return self;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    /*
    // Set window frame & background and make it visible
    */
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor clearColor];
    self.window.backgroundColor = [[UIColor alloc] initWithRed:51.0f green:51.0f blue:51.0f alpha:1.0f];
    [self.window makeKeyAndVisible];
    
    //init the main view with navController
    self.mainViewController = [[MainViewController alloc] init];
    
    //init the login view
    self.loginViewController = [[LoginViewController alloc] init];
    
    //init the split view
    self.splitViewController = [[SplitViewController alloc] init];
    
    /*
    // Test if user is logged and init MainView
    // Else show init with LoginView
    // init the splitView disposal
    */
    if(isLogged){
        self.insideNavigationController = [[UINavigationController alloc] initWithRootViewController:self.mainViewController];
        
        // DeckView init
        self.insideDeckViewController =  [[IIViewDeckController alloc] initWithCenterViewController:self.insideNavigationController
                                                                                 leftViewController:self.splitViewController
                                                                                rightViewController:nil];
        [self.insideDeckViewController setLeftSize: 80.f];
        self.insideDeckViewController.bounceOpenSideDurationFactor = 1.6f;
        
        self.window.rootViewController = self.insideDeckViewController;
    }else{
        self.insideNavigationController = [[UINavigationController alloc] initWithRootViewController:self.loginViewController];
        [self.insideNavigationController setNavigationBarHidden:YES];
        
        // DeckView init
        self.insideDeckViewController =  [[IIViewDeckController alloc] initWithCenterViewController:self.insideNavigationController
                                                                                 leftViewController:self.splitViewController
                                                                                rightViewController:nil];
        [self.insideDeckViewController setLeftSize: 80.f];
        [self.insideDeckViewController setPanningMode:IIViewDeckNoPanning];
        self.insideDeckViewController.bounceOpenSideDurationFactor = 1.6f;
        
        self.window.rootViewController = self.insideDeckViewController;
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"inside4paris" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"inside4paris.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
