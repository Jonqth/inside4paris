//
//  insideAppDelegate.h
//  inside4paris
//
//  Created by Jonathan Araujo-Levy on 18/10/12.
//  Copyright (c) 2012 hetic. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MainViewController;
@class LoginViewController;
@class IIViewDeckController;
@class SplitViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

// Vars
@property (assign, nonatomic) BOOL mainScreenI5;

// View
@property (strong, nonatomic) MainViewController *mainViewController;
@property (strong, nonatomic) LoginViewController *loginViewController;
@property (strong, nonatomic) IIViewDeckController *insideDeckViewController;
@property (strong, nonatomic) SplitViewController *splitViewController;

// Navigation elements
@property (strong, nonatomic) UINavigationController *insideNavigationController;

// Vars
@property (nonatomic, assign) BOOL isLogged;
@property (strong, nonatomic) UIFont *insideMainFont;
@property (strong, nonatomic) UIFont *insideTitleFont;
@property (strong, nonatomic) UIFont *insideBoldFont;
@property (strong, nonatomic) UIFont *insideTinyFont;
@property (strong, nonatomic) UIColor *insidePinkColor;
@property (strong, nonatomic) UIColor *insideGreyColor;

// CoreData
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

// Init methods
- (id)init;

// Core Data methods
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
