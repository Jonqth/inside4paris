//
//  insideDataSingleton.m
//  inside4paris
//
//  Created by Jonathan Araujo-Levy on 19/10/12.
//  Copyright (c) 2012 hetic. All rights reserved.
//

#import "DataSingleton.h"

@implementation DataSingleton

static DataSingleton *sharedDataObject = nil;


 /*
 // static sharedData
 // :NSObject
 */
+ (DataSingleton *) sharedData {
    if (sharedDataObject == nil) { sharedDataObject = [[super alloc] init]; }
    return sharedDataObject;
}

- (id)init
{
    self = [super init];
    if (self) {}
    return self;
}

+ (id)allocWithZone:(NSZone*)zone {
    return [self sharedData];
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

@end
