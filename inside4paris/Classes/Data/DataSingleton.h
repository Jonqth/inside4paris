//
//  insideDataSingleton.h
//  inside4paris
//
//  Created by Jonathan Araujo-Levy on 19/10/12.
//  Copyright (c) 2012 hetic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataSingleton : NSObject {
    
}

+ (id)sharedData;

@end
