//
//  LoginViewController.h
//  inside4paris
//
//  Created by Jonathan Araujo-Levy on 30/10/12.
//  Copyright (c) 2012 hetic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InsideViewController.h"

@interface LoginViewController : InsideViewController <UIScrollViewDelegate>

// UI elements
@property (strong, nonatomic) UIScrollView *sliderDisplayContain;
@property (strong, nonatomic) UIPageControl *sliderPageControl;

// Vars
@property (strong, nonatomic) NSMutableArray *sliderDataArray;
@property (nonatomic, assign) BOOL slideBeingUsed;

@end
