//
//  MainViewController.h
//  inside4paris
//
//  Created by Jonathan Araujo-Levy on 19/10/12.
//  Copyright (c) 2012 hetic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InsideViewController.h"
#import "InfoViewController.h"

@class InfoViewController;

@interface MainViewController : InsideViewController

// Views
@property (strong, nonatomic) InfoViewController *infoViewController;



@end
