//
//  LoginViewController.m
//  inside4paris
//
//  Created by Jonathan Araujo-Levy on 30/10/12.
//  Copyright (c) 2012 hetic. All rights reserved.
//

#import "AppDelegate.h"
#import "InsideViewController.h"
#import "LoginViewController.h"
#import "MainViewController.h"
#import "QuartzCore/QuartzCore.h"

@interface LoginViewController () { CGFloat lastContentOffset; int lastViewedSlide; NSTimer *sliderTimer; } @end

@implementation LoginViewController 


// UIElements
@synthesize sliderDisplayContain;
@synthesize sliderPageControl;

// Vars
@synthesize sliderDataArray;
@synthesize slideBeingUsed;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Login";
        
        // hide navigationBar from @superclass
        [super hideNavigationBar];

        [self initElements];
        [self initSlider];
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated {
    [self loginViewUILoading];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    slideBeingUsed = NO;
}

#pragma elements

- (void)initElements{
    sliderDataArray = [[NSMutableArray alloc] init];
    
    NSArray *firstSlide = [NSArray  arrayWithObjects:[NSString stringWithFormat:@"Découvrez le 4ème arrondissement de Paris, différemment."],
                          [UIImage imageNamed:@"i4p_firstSlideLogin.png"], nil];
    [sliderDataArray insertObject:firstSlide atIndex:0];
    NSArray *secondSlide = [NSArray  arrayWithObjects:[NSString stringWithFormat:@"Découvrez une activité gustative ou culturelle autour de vous."],
                            [UIImage imageNamed:@"i4p_secondSlideLogin.png"], nil];
    [sliderDataArray insertObject:secondSlide atIndex:1];
    NSArray *thirdSlide = [NSArray  arrayWithObjects:[NSString stringWithFormat:@"Suivez des itinéraires crées par l'office du tourisme"],
                           [UIImage imageNamed:@"i4p_thirdSlideLogin.png"], nil];
    [sliderDataArray insertObject:thirdSlide atIndex:2];
    NSArray *forthSlide = [NSArray  arrayWithObjects:[NSString stringWithFormat:@"Consultez des fiches sur les lieux et monuments"],
                           [UIImage imageNamed:@"i4p_forthSlideLogin.png"], nil];
    [sliderDataArray insertObject:forthSlide atIndex:3];
    NSArray *fifthSlide = [NSArray  arrayWithObjects:[NSString stringWithFormat:@"Déposez vos avis et consultez ceux des autres utilisateurs"],
                           [UIImage imageNamed:@"i4p_fifthSlideLogin.png"], nil];
    [sliderDataArray insertObject:fifthSlide atIndex:4];
}

#pragma events

 /*
 // Update the page when more than 50% of the previous/next page is visible
 // @return void
 */

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    if (!slideBeingUsed) {
        CGFloat pageWidth = sliderDisplayContain.frame.size.width;
        int page = floor((sliderDisplayContain.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        [self.sliderPageControl setCurrentPage:page];
    }
}

- (void) PageControlChanged {
    // invalidate Timer
    [self invalidateSlider];
    
    CGRect frame;
    frame.origin.x = self.sliderDisplayContain.frame.size.width * self.sliderPageControl.currentPage;
    frame.origin.y = 0;
    frame.size = self.sliderDisplayContain.frame.size;
    [self.sliderDisplayContain scrollRectToVisible:frame animated:YES];
    slideBeingUsed = YES;
    
    // fire Timer
    [self initSlider];

}

 // handle Swipe on slider
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    slideBeingUsed = NO;
}

- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    slideBeingUsed = NO;
    [self invalidateSlider];
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    slideBeingUsed = NO;
    [self initSlider];
}

#pragma slider

- (void) initSlider {
    if(lastViewedSlide == 0){lastViewedSlide = 1;}
    sliderTimer = [NSTimer scheduledTimerWithTimeInterval:4.f target:self selector:@selector(loginViewSlider) userInfo:nil repeats:YES];
}

- (void) invalidateSlider {
    [sliderTimer invalidate];
}

- (void) loginViewSlider {
    
    if(lastViewedSlide > 4){
        CGRect frame;
        frame.origin.x = self.sliderDisplayContain.frame.size.width * (0);
        frame.origin.y = 0;
        frame.size = self.sliderDisplayContain.frame.size;
        [self.sliderDisplayContain scrollRectToVisible:frame animated:YES];
        lastViewedSlide = 1;
    }else{
        CGRect frame;
        frame.origin.x = self.sliderDisplayContain.frame.size.width * (lastViewedSlide);
        frame.origin.y = 0;
        frame.size = self.sliderDisplayContain.frame.size;
        [self.sliderDisplayContain scrollRectToVisible:frame animated:YES];
        lastViewedSlide++;
    }
    
    [self.sliderPageControl setCurrentPage:lastViewedSlide+1];
}

 /*
 // Access app without connection
 // @return void
 */

- (void) bypassFBConnect {
    MainViewController *mainViewController = [[MainViewController alloc] init];
    [self.navigationController pushViewController:mainViewController animated:YES];
}


#pragma uiloading

 /*
 // Loading of the logged MainView
 // @return void
 */

- (void)loginViewUILoading {
    
    // LoginDisplay
    UIImage *loginDisplayImage = [UIImage imageNamed:@"i4p_loginDisplay.jpg"];
    UIImageView *loginDisplayImageView = [[UIImageView alloc] initWithImage:loginDisplayImage];
    loginDisplayImageView.frame = CGRectMake(0,(self.view.frame.size.height-loginDisplayImageView.frame.size.height)
                                             ,loginDisplayImageView.frame.size.width,loginDisplayImageView.frame.size.height);
    [self.view addSubview:loginDisplayImageView];
    
    // LoginSliderBackground
    UIImage *LoginSliderImageBackground = [UIImage imageNamed:@"i4p_loginSliderBackground.jpg"];
    UIImageView *LoginSliderImageView = [[UIImageView alloc] initWithImage:LoginSliderImageBackground];
    LoginSliderImageView.frame = CGRectMake(0,0
                                            ,LoginSliderImageView.frame.size.width,(self.view.frame.size.height-loginDisplayImageView.frame.size.height));
    LoginSliderImageView.layer.shadowColor = [UIColor blackColor].CGColor;
    LoginSliderImageView.layer.shadowOffset = CGSizeMake(0, 0.1);
    LoginSliderImageView.layer.shadowOpacity = 0.7;
    LoginSliderImageView.layer.shadowRadius = 0.6;
    LoginSliderImageView.clipsToBounds = NO;
    [self.view addSubview:LoginSliderImageView];
    
    
    // LoginSliderViews
    self.sliderDisplayContain = [[UIScrollView alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,LoginSliderImageView.frame.size.height)];
    [self.sliderDisplayContain setDelegate:self];
    [self.sliderDisplayContain setBackgroundColor:[UIColor clearColor]];
    [self.sliderDisplayContain setContentSize:CGSizeMake(self.sliderDisplayContain.frame.size.width * sliderDataArray.count, self.sliderDisplayContain.frame.size.height)];
    [self.sliderDisplayContain setShowsHorizontalScrollIndicator:NO];
    [self.sliderDisplayContain setAlwaysBounceHorizontal:NO];
    [self.sliderDisplayContain setClipsToBounds:YES];
    [self.sliderDisplayContain setPagingEnabled:YES];
    [self.sliderDisplayContain setCanCancelContentTouches:NO];
    [self.view addSubview:self.sliderDisplayContain];
    
    self.sliderPageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0,self.sliderDisplayContain.frame.size.height-45,self.view.frame.size.width,45)];
    [self.sliderPageControl setBackgroundColor:[UIColor clearColor]];
    [self.sliderPageControl setNumberOfPages:sliderDataArray.count];
    [self.sliderPageControl setCurrentPage:0];
    [self.sliderPageControl setPageIndicatorTintColor:[UIColor colorWithRed:(193/255.f) green:(193/255.f) blue:(193/255.f) alpha:1]];
    [self.sliderPageControl setCurrentPageIndicatorTintColor:sharedAppDelegate.insidePinkColor];
    [self.sliderPageControl addTarget:self action:@selector(PageControlChanged) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:self.sliderPageControl];
    
    for (int i = 0; i < sliderDataArray.count; i++) {
        UIView *sliderPage = [[UIView alloc] initWithFrame:CGRectMake((sliderDisplayContain.frame.size.width * i),0
                                                                           ,sliderDisplayContain.frame.size.width,sliderDisplayContain.frame.size.height-50)];
        UIImageView *sliderImage = [[UIImageView alloc] initWithImage:[[sliderDataArray objectAtIndex:i] objectAtIndex:1]];
        
        //iPhone5
        CGRect frame;
        if(sharedAppDelegate.mainScreenI5){
             frame = CGRectMake(sliderPage.frame.size.width/2-sliderImage.frame.size.width/2, (sliderImage.frame.size.height/2), sliderImage.frame.size.width, sliderImage.frame.size.width);
        }
        else {
             frame = CGRectMake(sliderPage.frame.size.width/2-sliderImage.frame.size.width/2, 30, sliderImage.frame.size.width, sliderImage.frame.size.width);
        }
        
        sliderImage.frame = frame;
        
        UILabel *sliderTitle = [[UILabel alloc] initWithFrame:CGRectMake((25+(sliderDisplayContain.frame.size.width * 0)),sliderPage.frame.size.height-50,270,50)];
        [sliderTitle setBackgroundColor:[UIColor clearColor]];
        [sliderTitle setTextAlignment:NSTextAlignmentCenter];
        [sliderTitle setNumberOfLines:2];
        [sliderTitle setTextColor:sharedAppDelegate.insidePinkColor];
        [sliderTitle setText:[[sliderDataArray objectAtIndex:i] objectAtIndex:0]];
        [sliderTitle setFont:sharedAppDelegate.insideMainFont];
        [sliderTitle setAdjustsFontSizeToFitWidth:YES];

        [sliderPage addSubview:sliderImage];
        [sliderPage addSubview:sliderTitle];
        [sliderDisplayContain addSubview:sliderPage];
    }
    
    // NormalConnectButton
    UIButton *NMConnectButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *NMConnectButtonImage = [UIImage imageNamed:@"i4p_nmButton.png"];
    [NMConnectButton setImage:NMConnectButtonImage forState:UIControlStateNormal];
    NMConnectButton.frame = CGRectMake((self.view.frame.size.width/2-(NMConnectButtonImage.size.width/2)),
                                       self.view.frame.size.height-(NMConnectButtonImage.size.height+20), NMConnectButtonImage.size.width, NMConnectButtonImage.size.height);
    [NMConnectButton setTitle:@"Normal" forState:UIControlStateNormal];
    [NMConnectButton addTarget:self action:@selector(bypassFBConnect) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:NMConnectButton];
    
    // FacebookConnectButton
    UIButton *FBConnectButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *FBConnectButtonImage = [UIImage imageNamed:@"i4p_fbConnectButton.png"];
    [FBConnectButton setImage:FBConnectButtonImage forState:UIControlStateNormal];
    FBConnectButton.frame = CGRectMake((self.view.frame.size.width/2-(FBConnectButtonImage.size.width/2)),
                                       NMConnectButton.frame.origin.y-(FBConnectButtonImage.size.height+20), FBConnectButtonImage.size.width, FBConnectButtonImage.size.height);
    [FBConnectButton addTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:FBConnectButton];
    
}

#pragma memory

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
