//
//  InfoViewController.m
//  inside4paris
//
//  Created by Jonathan Araujo-Levy on 14/11/12.
//  Copyright (c) 2012 hetic. All rights reserved.
//

#import "AppDelegate.h"
#import "InfoViewController.h"
#import "QuartzCore/QuartzCore.h"

@interface InfoViewController () {
    float sizeDividei5;
}

@end

@implementation InfoViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [super setExitButton];
    [super.mainNavigationItem setTitleView:nil];
    [super SetTextualTitleView:@"À PROPOS"];
    
    [self infoViewUILoading];
}

#pragma uiloading

 /*
 // Loading of the infoView
 // @return void
 */

- (void)infoViewUILoading {
    
    // @declarations
    UIImage *infoDisplayImage = [UIImage imageNamed:@"i4p_infoDisplay.png"];
    UIImageView *infoDisplayView = [[UIImageView alloc] initWithFrame:CGRectMake(0, super.mainNavigationBar.frame.size.height,
                                                                                 self.view.frame.size.width, infoDisplayImage.size.height)];
    [infoDisplayView setImage:infoDisplayImage];
    
    // infoLogoAboveDisplayView
    if(sharedAppDelegate.mainScreenI5){ sizeDividei5 = 1; }
    else{ sizeDividei5 = 1.2; }
    
    UIImage *infoLogoAboveDisplayImage = [UIImage imageNamed:@"i4p_firstSlideLogin.png"];
    UIImageView *infoLogoAboveDisplayView = [[UIImageView alloc] initWithFrame:CGRectMake(
                                                                            (self.view.frame.size.width/2)-((infoLogoAboveDisplayImage.size.width/(sizeDividei5))/2),
                                                                            (infoDisplayView.frame.size.height-infoLogoAboveDisplayImage.size.height/3),
                                                                             infoLogoAboveDisplayImage.size.width/(sizeDividei5),infoLogoAboveDisplayImage.size.height/(sizeDividei5))];
    [infoLogoAboveDisplayView setImage:infoLogoAboveDisplayImage];
    
    [self.view addSubview:infoDisplayView];
    [self.view addSubview:infoLogoAboveDisplayView];
    
    // infoTitleText
    UILabel *infoTitleText = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2)-(self.view.frame.size.width/1.5)/2,
                                                                       infoLogoAboveDisplayView.frame.origin.y+infoLogoAboveDisplayView.frame.size.height/sizeDividei5+10,
                                                                       self.view.frame.size.width/1.5, 50)];
    [infoTitleText setText:@"Découvrez le 4ème arrondissement de Paris différemment"];
    [infoTitleText setFont:sharedAppDelegate.insideMainFont];
    [infoTitleText setNumberOfLines:2];
    [infoTitleText setTextColor:sharedAppDelegate.insidePinkColor];
    [infoTitleText setBackgroundColor:[UIColor clearColor]];
    [infoTitleText setTextAlignment:NSTextAlignmentCenter];
    
    [self.view addSubview:infoTitleText];
    
    // infoMiddleText
    UILabel *infoMiddleText = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2)-(self.view.frame.size.width/1.5)/2,
                                                                       infoTitleText.frame.origin.y+infoTitleText.frame.size.height-5,
                                                                       self.view.frame.size.width/1.5, 50)];
    
    [infoMiddleText setText:@"Inside4Paris c'est aussi un syndicat d'initiative. N'hésitez pas à nous rendre visite !"];
    [infoMiddleText setFont:sharedAppDelegate.insideMainFont];
    [infoMiddleText setNumberOfLines:3];
    [infoMiddleText setTextColor:sharedAppDelegate.insideGreyColor];
    [infoMiddleText setBackgroundColor:[UIColor clearColor]];
    [infoMiddleText setTextAlignment:NSTextAlignmentCenter];
    
    [self.view addSubview:infoMiddleText];
    
    // infoTitleBold
    UILabel *infoTitleBold = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2)-(self.view.frame.size.width/1.5)/2,
                                                                        infoTitleText.frame.origin.y+infoTitleText.frame.size.height+40,
                                                                        self.view.frame.size.width/1.5, 50)];
    [infoTitleBold setText:@"Informations"];
    [infoTitleBold setBackgroundColor:[UIColor clearColor]];
    [infoTitleBold setTextAlignment:NSTextAlignmentCenter];
    [infoTitleBold setFont:sharedAppDelegate.insideBoldFont];
    [infoTitleBold setTextColor:sharedAppDelegate.insideGreyColor];
    
    [self.view addSubview:infoTitleBold];
    
    // infoDescriptionText
    UILabel *infoDescriptionText = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2)-(self.view.frame.size.width/1.5)/2,
                                                                       infoTitleBold.frame.origin.y+infoTitleBold.frame.size.height-20,
                                                                       self.view.frame.size.width/1.5, 50)];
    [infoDescriptionText setText:@"25 Rue des Pyramides Du lundi au vendredi de 9h à 17h"];
    [infoDescriptionText setFont:sharedAppDelegate.insideMainFont];
    [infoDescriptionText setNumberOfLines:2];
    [infoDescriptionText setTextColor:sharedAppDelegate.insidePinkColor];
    [infoDescriptionText setBackgroundColor:[UIColor clearColor]];
    [infoDescriptionText setTextAlignment:NSTextAlignmentCenter];
    
    [self.view addSubview:infoDescriptionText];
    
    
    // partnerTitleBold
    UILabel *partnerTitleBold = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2)-(self.view.frame.size.width/1.5)/2,
                                                                       infoDescriptionText.frame.origin.y+infoDescriptionText.frame.size.height-15,
                                                                       self.view.frame.size.width/1.5, 50)];
    [partnerTitleBold setText:@"Partenaires"];
    [partnerTitleBold setBackgroundColor:[UIColor clearColor]];
    [partnerTitleBold setTextAlignment:NSTextAlignmentCenter];
    [partnerTitleBold setFont:sharedAppDelegate.insideBoldFont];
    [partnerTitleBold setTextColor:sharedAppDelegate.insideGreyColor];
    
    [self.view addSubview:partnerTitleBold];
    
    // infoPartnerView
    UIImage *infoPartnerImage = [UIImage imageNamed:@"i4p_partnerLogosDisplay.png"];
    UIImageView *infoPartnerImageView = [[UIImageView alloc] initWithImage:infoPartnerImage];
    [infoPartnerImageView setFrame:CGRectMake(0, 10, infoPartnerImage.size.width, infoPartnerImage.size.height)];
    
    UIScrollView *infoPartnerView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, (self.view.frame.size.height-55), self.view.frame.size.width, 55)];
    [infoPartnerView setBackgroundColor:[UIColor colorWithRed:(218/255.f) green:(218/255.f) blue:(218/255.f) alpha:1]];
    [infoPartnerView setContentSize:CGSizeMake(infoPartnerImageView.frame.size.width, infoPartnerImageView.frame.size.height)];
    
    infoPartnerView.layer.shadowColor = [UIColor blackColor].CGColor;
    infoPartnerView.layer.shadowOffset = CGSizeMake(0, 0.1);
    infoPartnerView.layer.shadowOpacity = 0.7;
    infoPartnerView.layer.shadowRadius = 0.6;
    infoPartnerView.clipsToBounds = NO;
    
    [infoPartnerView setShowsHorizontalScrollIndicator:NO];
    [infoPartnerView addSubview:infoPartnerImageView];
    
    [self.view addSubview:infoPartnerView];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
