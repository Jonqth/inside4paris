//
//  PlacesSingleViewController.m
//  inside4paris
//
//  Created by Jonathan Araujo-Levy on 20/12/12.
//  Copyright (c) 2012 hetic. All rights reserved.
//

#import "PlacesSingleViewController.h"

@interface PlacesSingleViewController ()

@end

@implementation PlacesSingleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {}
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setBackButton];
    
	UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2)-75, 100, 150, 50)];
    [myLabel setText:@"Le restaurant du port"];
    [myLabel setBackgroundColor:[UIColor clearColor]];
    [myLabel setTextColor:sharedAppDelegate.insidePinkColor];
    [self.view addSubview:myLabel];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
