//
//  SplitViewController.m
//  inside4paris
//
//  Created by Jonathan Araujo-Levy on 24/11/12.
//  Copyright (c) 2012 hetic. All rights reserved.
//

#import "SplitViewController.h"
#import "AppDelegate.h"
#import "QuartzCore/QuartzCore.h"

@interface SplitViewController () { NSMutableArray *arrayForMenuSettings; } @end

@implementation SplitViewController

// Views
@synthesize menuTableView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        [self.view setBackgroundColor:[UIColor colorWithRed:(58/255.0f) green:(58/255.0f) blue:(58/255.0f) alpha:1]];

        //Initialize the array.
        arrayForMenuSettings = [[NSMutableArray alloc] init];
        
        /*
        // Adding titles
        // for separate sections
        */
        
        // MenuHomeSettings
        NSArray *menuHomeFirstCell   = [NSArray arrayWithObjects:[NSString stringWithFormat:@"Accueil"], [UIImage imageNamed:@"i4p_deckHomeButton.png"], nil];
        NSArray *menuHomeSecondCell  = [NSArray arrayWithObjects:[NSString stringWithFormat:@"Lieux"], [UIImage imageNamed:@"i4p_deckHereButton.png"], nil];
        NSArray *menuHomeThirdCell   = [NSArray arrayWithObjects:[NSString stringWithFormat:@"Itinéraires"], [UIImage imageNamed:@"i4p_deckItineraryButton.png"], nil];
        NSArray *menuHomeForthCell   = [NSArray arrayWithObjects:[NSString stringWithFormat:@"Evénements"], [UIImage imageNamed:@"i4p_deckEventButton.png"], nil];
        NSArray *menuHomeFifthCell   = [NSArray arrayWithObjects:[NSString stringWithFormat:@"Livre d'or"],[UIImage imageNamed:@"i4p_deckGoldenButton.png"], nil];
        //NSArray *menuHomeSixthCell   = [NSArray arrayWithObjects:[NSString stringWithFormat:@"Favoris"], [UIImage imageNamed:@"i4p_deckGoldenButton.png"], nil];
        
        NSArray *menuHomeList = [NSArray arrayWithObjects:menuHomeFirstCell, menuHomeSecondCell, menuHomeThirdCell, menuHomeForthCell, menuHomeFifthCell,
                                                          nil];

        NSArray *menuHomeHeader = [NSArray arrayWithObjects:[NSString stringWithFormat:@"MENU"], menuHomeList, nil];
        
        [arrayForMenuSettings addObject:menuHomeHeader];
        
        // MenuSettingsSettings
        NSArray *menuSettingsFirstCell = [NSArray arrayWithObjects:[NSString stringWithFormat:@"Deconnexion"], [UIImage imageNamed:@"i4p_exitButtonNormal.png"], nil];
        NSArray *menuSettingsList = [NSArray arrayWithObjects:menuSettingsFirstCell, nil];
        NSArray *menuSettingsHeader = [NSArray arrayWithObjects:[NSString stringWithFormat:@"REGLAGES"], menuSettingsList, nil];
        
        [arrayForMenuSettings addObject:menuSettingsHeader];
        
        // MenuLanguageSettings
        NSArray *menuLanguageFirstCell = [NSArray arrayWithObjects:[NSString stringWithFormat:@"Choose"], [UIImage imageNamed:@"i4p_exitButtonNormal.png"], nil];
        NSArray *menuLanguageList = [NSArray arrayWithObjects:menuLanguageFirstCell, nil];
        NSArray *menuLanguageHeader = [NSArray arrayWithObjects:[NSString stringWithFormat:@"LANGUE"], menuLanguageList, nil];
        
        [arrayForMenuSettings addObject:menuLanguageHeader];
        
        // MenuCreditsSettings
        NSArray *menuCreditsFirstCell = [NSArray arrayWithObjects:[NSString stringWithFormat:@"Mentions légales"], [UIImage imageNamed:@"i4p_exitButtonNormal.png"], nil];
        NSArray *menuCreditsSecondCell = [NSArray arrayWithObjects:[NSString stringWithFormat:@"Crédits"], [UIImage imageNamed:@"i4p_exitButtonNormal.png"], nil];
        NSArray *menuCreditsList = [NSArray arrayWithObjects:menuCreditsFirstCell, menuCreditsSecondCell,nil];
        NSArray *menuCreditsHeader = [NSArray arrayWithObjects:[NSString stringWithFormat:@"MENTIONS LEGALES"], menuCreditsList, nil];
        
        [arrayForMenuSettings addObject:menuCreditsHeader];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self menuUILoading];
}

/*
// TableView init
// initiate the menuTableView
*/

- (void) menuUILoading {
    
    // menuTableView
    self.menuTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    [self.menuTableView setBackgroundColor:[UIColor clearColor]];
    [self.menuTableView setDelegate:self];
    [self.menuTableView setDataSource:self];
    [self.menuTableView setSeparatorColor:[UIColor colorWithRed:(50/255.f) green:(50/255.f) blue:(50/255.f) alpha:1]];
    
    [self.view addSubview:self.menuTableView];
}

/*
////////////////////////////////////////////////////////////////////////
// TableView Methods
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // Push to dynamic view
    NSString *className = [[[[arrayForMenuSettings objectAtIndex:indexPath.section] objectAtIndex:1] objectAtIndex:indexPath.row] objectAtIndex:0];
    UIViewController *toController = [self pushFromMenuView:className];
    
    if(toController != nil){
        [sharedAppDelegate.insideDeckViewController closeLeftViewBouncing:^(IIViewDeckController *controller){
            [sharedAppDelegate.insideNavigationController pushViewController:toController animated:YES];
        }];
    }else if ([className isEqualToString:@"Deconnexion"]){
        [sharedAppDelegate.insideDeckViewController closeLeftViewBouncing:^(IIViewDeckController *controller){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Déconnexion"
                                                            message:@"Voulez-vous réellement vous déconnecter ?"
                                                           delegate:self
                                                  cancelButtonTitle:@"Non"
                                                  otherButtonTitles:nil];
            // optional - add more buttons:
            [alert addButtonWithTitle:@"Oui"];
            [alert show];
        }];
    }else if ([className isEqualToString:@"Choose"]){
        [sharedAppDelegate.insideDeckViewController closeLeftViewBouncing:^(IIViewDeckController *controller){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Langue"
                                                            message:@"Pas encore disponible"
                                                           delegate:self
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
            [alert show];
        }];
    }

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    // Customizing menuTableView    
    UIView *menuHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44.f)];
    [menuHeaderView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"i4p_menuHeaderBg.png"]]];
    menuHeaderView.layer.shadowColor = [UIColor blackColor].CGColor;
    menuHeaderView.layer.shadowOffset = CGSizeMake(0, 0.2);
    menuHeaderView.layer.shadowOpacity = 0.7;
    menuHeaderView.layer.shadowRadius = 0.6;
    menuHeaderView.clipsToBounds = NO;
    
    UILabel *menuHeaderSectionTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, menuHeaderView.frame.size.width, menuHeaderView.frame.size.height)];
    [menuHeaderSectionTitle setText:[[arrayForMenuSettings objectAtIndex:section] objectAtIndex:0]];
    [menuHeaderSectionTitle setBackgroundColor:[UIColor clearColor]];
    [menuHeaderSectionTitle setTextColor:[UIColor whiteColor]];
    [menuHeaderView addSubview:menuHeaderSectionTitle];

    return menuHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44.f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return [arrayForMenuSettings count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    // If you're serving data from an array, return the length of the array:
    return [[[arrayForMenuSettings objectAtIndex:section] objectAtIndex:1] count];

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.f;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setAccessoryType:UITableViewCellAccessoryDetailDisclosureButton];
    }
    
    
    if(indexPath.section == 0){
        // Set Cell's backgroundView
        UIView *menuView = [[UIView alloc] initWithFrame:cell.frame];
    
        // Set icon for menu
        UIImageView *IconMenuView = [[UIImageView alloc] initWithImage:[[[[arrayForMenuSettings objectAtIndex:0] objectAtIndex:1] objectAtIndex:indexPath.row] objectAtIndex:1]];
        [IconMenuView setFrame:CGRectMake(5, 4, IconMenuView.frame.size.width, IconMenuView.frame.size.height)];
        [menuView addSubview:IconMenuView];
    
        // Set the data for this cell
        UILabel *menuText = [[UILabel alloc] init];
        [menuText setText:[[[[arrayForMenuSettings objectAtIndex:indexPath.section] objectAtIndex:1] objectAtIndex:indexPath.row] objectAtIndex:0]];
        [menuText setFrame:CGRectMake(IconMenuView.frame.origin.x+(IconMenuView.frame.size.width+7), -1, 120, menuView.frame.size.height)];
        [menuText setBackgroundColor:[UIColor clearColor]];
        [menuText setTextColor:[UIColor whiteColor]];
        [menuView addSubview:menuText];
        
        // Set text for this cell
        [cell setBackgroundView:menuView];
    }else {
        
        // Set the data for this cell
        UILabel *menuText = [[UILabel alloc] initWithFrame:cell.frame];
        [menuText setText:[[[[arrayForMenuSettings objectAtIndex:indexPath.section] objectAtIndex:1] objectAtIndex:indexPath.row] objectAtIndex:0]];
        [menuText setBackgroundColor:[UIColor clearColor]];
        [menuText setTextColor:[UIColor whiteColor]];
        [cell addSubview:menuText];
    }

    
    // Set Shadow for this cell
    cell.layer.shadowColor = [UIColor whiteColor].CGColor;
    cell.layer.shadowOffset = CGSizeMake(0, 0.1);
    cell.layer.shadowOpacity = 0.1;
    cell.layer.shadowRadius = 0.6;
    cell.clipsToBounds = NO;
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    
    return cell;
}

/*
// TableView Methods
////////////////////////////////////////////////////////////////////////
*/


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
