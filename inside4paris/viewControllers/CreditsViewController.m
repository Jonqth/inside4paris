//
//  CreditsViewController.m
//  inside4paris
//
//  Created by Jonathan Araujo-Levy on 19/12/12.
//  Copyright (c) 2012 hetic. All rights reserved.
//

#import "CreditsViewController.h"

@interface CreditsViewController ()

@end

@implementation CreditsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self creditsUILoading];
	
    UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-75, 50, 150, 50)];
    [myLabel setText:@"Crédits"];
    [myLabel setBackgroundColor:[UIColor clearColor]];
    [myLabel setTextAlignment:NSTextAlignmentCenter];
    [myLabel setTextColor:sharedAppDelegate.insidePinkColor];
    [self.view addSubview:myLabel];
    
}

- (void) creditsUILoading {
    [self setMenuButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
