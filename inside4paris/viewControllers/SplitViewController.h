//
//  SplitViewController.h
//  inside4paris
//
//  Created by Jonathan Araujo-Levy on 24/11/12.
//  Copyright (c) 2012 hetic. All rights reserved.
//

#import "InsideSplitViewController.h"
#import "AppDelegate.h"

@interface SplitViewController : InsideSplitViewController <UITableViewDelegate, UITableViewDataSource>


// Views
@property (strong, nonatomic) UITableView *menuTableView;



@end
