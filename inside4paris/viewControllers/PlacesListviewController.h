//
//  PlacesListviewController.h
//  inside4paris
//
//  Created by Jonathan Araujo-Levy on 18/12/12.
//  Copyright (c) 2012 hetic. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "InsideViewController.h"

@interface PlacesListviewController : InsideViewController <UITableViewDelegate,UITableViewDataSource,MKMapViewDelegate,MKAnnotation,MKOverlay,CLLocationManagerDelegate>

@property (strong, nonatomic) UITableView *placesTableView;
@property (strong, nonatomic) MKMapView *placesMapView;
@property (strong, nonatomic) CLLocationManager *placesLocationManager;

@end
