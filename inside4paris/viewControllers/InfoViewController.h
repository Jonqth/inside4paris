//
//  InfoViewController.h
//  inside4paris
//
//  Created by Jonathan Araujo-Levy on 14/11/12.
//  Copyright (c) 2012 hetic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InsideViewController.h"

@interface InfoViewController : InsideViewController <UIScrollViewDelegate>

@end
