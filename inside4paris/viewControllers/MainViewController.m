//
//  MainViewController.m
//  inside4paris
//
//  Created by Jonathan Araujo-Levy on 19/10/12.
//  Copyright (c) 2012 hetic. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"

// Views
#import "PlacesListviewController.h"
#import "ItinerariesListViewController.h"
#import "EventListViewController.h"
#import "InfoViewController.h"

@interface MainViewController () {
    float firstlineY;
    float secondlineY;
    float thirdLineY;
}
    
@end

@implementation MainViewController


// Views
@synthesize infoViewController;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

-(void) viewWillAppear:(BOOL)animated {}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self mainViewUILoading];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma uiloading

/*
// Loading of the logged MainView
// @return void
*/

- (void)mainViewUILoading {
    
    [super setMenuButton];
    
    self.infoViewController = [[InfoViewController alloc] init];
    
    /////////////////////////////
    // Main Menu Loading
    /////////////////////////////
    
    // iPhones constraints placing
    if(sharedAppDelegate.mainScreenI5){
        // iPhone 5
        firstlineY = 70;
        secondlineY = 235;
        thirdLineY = 400;
    }else{
        // iPhone 4/4S
        firstlineY = 55;
        secondlineY = 190;
        thirdLineY = 325;
    }
    
    // Host
    UIButton *hostMainButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *hostMainButtonImage = [UIImage imageNamed:@"i4p_hostMainIcon.png"];
    
    [hostMainButton setTitle:@"Host" forState:UIControlStateNormal];
    [hostMainButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [hostMainButton setFrame:CGRectMake(15, firstlineY, hostMainButtonImage.size.width, hostMainButtonImage.size.height)];
    [hostMainButton setBackgroundImage:hostMainButtonImage forState:UIControlStateNormal];
    [hostMainButton addTarget:self action:@selector(pushToView:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *hostMainLabel = [[UILabel alloc] initWithFrame:CGRectMake(hostMainButton.frame.origin.x, (hostMainButton.frame.origin.y+(hostMainButton.frame.size.height+3)),
                                                                       hostMainButton.frame.size.width, hostMainButton.frame.size.height/2)];
    [hostMainLabel setTextAlignment:NSTextAlignmentCenter];
    [hostMainLabel setBackgroundColor:[UIColor clearColor]];
    [hostMainLabel setTextColor:sharedAppDelegate.insideGreyColor];
    [hostMainLabel setFont:sharedAppDelegate.insideMainFont];
    [hostMainLabel setText:@"Hébergement"];
    
    [self.view addSubview:hostMainLabel];
    [self.view addSubview:hostMainButton];
    
    // Culture
    UIButton *cultureMainButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *cultureMainButtonImage = [UIImage imageNamed:@"i4p_cultureMainIcon.png"];
    
    [cultureMainButton setTitle:@"Culture" forState:UIControlStateNormal];
    [cultureMainButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [cultureMainButton setFrame:CGRectMake((hostMainButton.frame.origin.x+(hostMainButton.frame.size.width+20)),
                                           hostMainButton.frame.origin.y, cultureMainButtonImage.size.width, cultureMainButtonImage.size.height)];
    [cultureMainButton setBackgroundImage:cultureMainButtonImage forState:UIControlStateNormal];
    [cultureMainButton addTarget:self action:@selector(pushToView:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *cultureMainLabel = [[UILabel alloc] initWithFrame:CGRectMake(cultureMainButton.frame.origin.x,
                                                                          (cultureMainButton.frame.origin.y+(cultureMainButton.frame.size.height+3)),
                                                                       cultureMainButton.frame.size.width, cultureMainButton.frame.size.height/2)];
    [cultureMainLabel setTextAlignment:NSTextAlignmentCenter];
    [cultureMainLabel setBackgroundColor:[UIColor clearColor]];
    [cultureMainLabel setTextColor:sharedAppDelegate.insideGreyColor];
    [cultureMainLabel setFont:sharedAppDelegate.insideMainFont];
    [cultureMainLabel setText:@"Culture"];
    
    [self.view addSubview:cultureMainLabel];
    [self.view addSubview:cultureMainButton];
    
    
    // Sales
    UIButton *salesMainButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *salesMainButtonImage = [UIImage imageNamed:@"i4p_salesMainIcon.png"];
    
    [salesMainButton setTitle:@"Sale" forState:UIControlStateNormal];
    [salesMainButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [salesMainButton setFrame:CGRectMake((cultureMainButton.frame.origin.x+(cultureMainButton.frame.size.width+20)),
                                           hostMainButton.frame.origin.y, salesMainButtonImage.size.width, salesMainButtonImage.size.height)];
    [salesMainButton setBackgroundImage:salesMainButtonImage forState:UIControlStateNormal];
    [salesMainButton addTarget:self action:@selector(pushToView:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *salesMainLabel = [[UILabel alloc] initWithFrame:CGRectMake(salesMainButton.frame.origin.x,
                                                                          (salesMainButton.frame.origin.y+(salesMainButton.frame.size.height+3)),
                                                                          salesMainButton.frame.size.width, salesMainButton.frame.size.height/2)];
    [salesMainLabel setTextAlignment:NSTextAlignmentCenter];
    [salesMainLabel setBackgroundColor:[UIColor clearColor]];
    [salesMainLabel setTextColor:sharedAppDelegate.insideGreyColor];
    [salesMainLabel setFont:sharedAppDelegate.insideMainFont];
    [salesMainLabel setText:@"Commerces"];
    
    [self.view addSubview:salesMainLabel];
    [self.view addSubview:salesMainButton];
    
    // 2ND LINE
    ///////////////////
    
    // Food
    UIButton *foodMainButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *foodMainButtonImage = [UIImage imageNamed:@"i4p_foodMainIcon.png"];
    
    [foodMainButton setTitle:@"Food" forState:UIControlStateNormal];
    [foodMainButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [foodMainButton setFrame:CGRectMake((hostMainButton.frame.origin.x),
                                         secondlineY, foodMainButtonImage.size.width, foodMainButtonImage.size.height)];
    [foodMainButton setBackgroundImage:foodMainButtonImage forState:UIControlStateNormal];
    [foodMainButton addTarget:self action:@selector(pushToView:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *foodMainLabel = [[UILabel alloc] initWithFrame:CGRectMake(foodMainButton.frame.origin.x,
                                                                        (foodMainButton.frame.origin.y+(foodMainButton.frame.size.height+3)),
                                                                        foodMainButton.frame.size.width, foodMainButton.frame.size.height/2)];
    [foodMainLabel setTextAlignment:NSTextAlignmentCenter];
    [foodMainLabel setBackgroundColor:[UIColor clearColor]];
    [foodMainLabel setTextColor:sharedAppDelegate.insideGreyColor];
    [foodMainLabel setFont:sharedAppDelegate.insideMainFont];
    [foodMainLabel setText:@"Restaurants"];
    
    [self.view addSubview:foodMainLabel];
    [self.view addSubview:foodMainButton];
    
    // Bars
    UIButton *barsMainButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *barsMainButtonImage = [UIImage imageNamed:@"i4p_barsMainIcon.png"];
    
    [barsMainButton setTitle:@"Bar" forState:UIControlStateNormal];
    [barsMainButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [barsMainButton setFrame:CGRectMake((foodMainButton.frame.origin.x+(foodMainButton.frame.size.width+20)),
                                         foodMainButton.frame.origin.y, barsMainButtonImage.size.width, barsMainButtonImage.size.height)];
    [barsMainButton setBackgroundImage:barsMainButtonImage forState:UIControlStateNormal];
    [barsMainButton addTarget:self action:@selector(pushToView:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *barsMainLabel = [[UILabel alloc] initWithFrame:CGRectMake(barsMainButton.frame.origin.x,
                                                                        (barsMainButton.frame.origin.y+(barsMainButton.frame.size.height+3)),
                                                                        barsMainButton.frame.size.width, barsMainButton.frame.size.height/2)];
    [barsMainLabel setTextAlignment:NSTextAlignmentCenter];
    [barsMainLabel setBackgroundColor:[UIColor clearColor]];
    [barsMainLabel setTextColor:sharedAppDelegate.insideGreyColor];
    [barsMainLabel setFont:sharedAppDelegate.insideMainFont];
    [barsMainLabel setText:@"Bars"];
    
    [self.view addSubview:barsMainLabel];
    [self.view addSubview:barsMainButton];
    
    // coffees
    UIButton *coffeesMainButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *coffeesMainButtonImage = [UIImage imageNamed:@"i4p_cofeeMainIcon.png"];
    
    [coffeesMainButton setTitle:@"Coffee" forState:UIControlStateNormal];
    [coffeesMainButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [coffeesMainButton setFrame:CGRectMake((barsMainButton.frame.origin.x+(barsMainButton.frame.size.width+20)),
                                        foodMainButton.frame.origin.y, coffeesMainButtonImage.size.width, coffeesMainButtonImage.size.height)];
    [coffeesMainButton setBackgroundImage:coffeesMainButtonImage forState:UIControlStateNormal];
    [coffeesMainButton addTarget:self action:@selector(pushToView:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *coffeesMainLabel = [[UILabel alloc] initWithFrame:CGRectMake(coffeesMainButton.frame.origin.x,
                                                                       (coffeesMainButton.frame.origin.y+(coffeesMainButton.frame.size.height+3)),
                                                                       coffeesMainButton.frame.size.width, coffeesMainButton.frame.size.height/2)];
    [coffeesMainLabel setTextAlignment:NSTextAlignmentCenter];
    [coffeesMainLabel setBackgroundColor:[UIColor clearColor]];
    [coffeesMainLabel setTextColor:sharedAppDelegate.insideGreyColor];
    [coffeesMainLabel setFont:sharedAppDelegate.insideMainFont];
    [coffeesMainLabel setText:@"Cafés"];
    
    [self.view addSubview:coffeesMainLabel];
    [self.view addSubview:coffeesMainButton];
    
    // 3RD LINE
    ///////////////////
    
    // Events
    UIButton *eventsMainButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *eventsMainButtonImage = [UIImage imageNamed:@"i4p_eventMainIcon.png"];
    
    [eventsMainButton setTitle:@"Event" forState:UIControlStateNormal];
    [eventsMainButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [eventsMainButton setFrame:CGRectMake((hostMainButton.frame.origin.x),
                                        thirdLineY, eventsMainButtonImage.size.width, eventsMainButtonImage.size.height)];
    [eventsMainButton setBackgroundImage:eventsMainButtonImage forState:UIControlStateNormal];
    [eventsMainButton addTarget:self action:@selector(pushToView:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *eventsMainLabel = [[UILabel alloc] initWithFrame:CGRectMake(eventsMainButton.frame.origin.x,
                                                                       (eventsMainButton.frame.origin.y+(eventsMainButton.frame.size.height+3)),
                                                                       eventsMainButton.frame.size.width, eventsMainButton.frame.size.height/2)];
    [eventsMainLabel setTextAlignment:NSTextAlignmentCenter];
    [eventsMainLabel setBackgroundColor:[UIColor clearColor]];
    [eventsMainLabel setTextColor:sharedAppDelegate.insideGreyColor];
    [eventsMainLabel setFont:sharedAppDelegate.insideMainFont];
    [eventsMainLabel setText:@"Événements"];
    
    [self.view addSubview:eventsMainLabel];
    [self.view addSubview:eventsMainButton];
    
    // Paths
    UIButton *pathMainButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *pathMainButtonImage = [UIImage imageNamed:@"i4p_pathMainIcon.png"];
    
    [pathMainButton setTitle:@"Path" forState:UIControlStateNormal];
    [pathMainButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [pathMainButton setFrame:CGRectMake((eventsMainButton.frame.origin.x+(eventsMainButton.frame.size.width+20)),
                                        eventsMainButton.frame.origin.y, pathMainButtonImage.size.width, pathMainButtonImage.size.height)];
    [pathMainButton setBackgroundImage:pathMainButtonImage forState:UIControlStateNormal];
    [pathMainButton addTarget:self action:@selector(pushToView:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *pathMainLabel = [[UILabel alloc] initWithFrame:CGRectMake(pathMainButton.frame.origin.x,
                                                                       (pathMainButton.frame.origin.y+(pathMainButton.frame.size.height+3)),
                                                                       pathMainButton.frame.size.width, pathMainButton.frame.size.height/2)];
    [pathMainLabel setTextAlignment:NSTextAlignmentCenter];
    [pathMainLabel setBackgroundColor:[UIColor clearColor]];
    [pathMainLabel setTextColor:sharedAppDelegate.insideGreyColor];
    [pathMainLabel setFont:sharedAppDelegate.insideMainFont];
    [pathMainLabel setText:@"Itinéraires"];
    
    [self.view addSubview:pathMainLabel];
    [self.view addSubview:pathMainButton];
    
    // Syndic
    UIButton *syndicMainButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *syndicMainButtonImage = [UIImage imageNamed:@"i4p_syndicMainIcon.png"];
    
    [syndicMainButton setTitle:@"Syndicat" forState:UIControlStateNormal];
    [syndicMainButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    
    [syndicMainButton setFrame:CGRectMake((pathMainButton.frame.origin.x+(pathMainButton.frame.size.width+20)),
                                        eventsMainButton.frame.origin.y, syndicMainButtonImage.size.width, syndicMainButtonImage.size.height)];
    [syndicMainButton setBackgroundImage:syndicMainButtonImage forState:UIControlStateNormal];
    [syndicMainButton addTarget:self action:@selector(pushToView:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *syndicMainLabel = [[UILabel alloc] initWithFrame:CGRectMake(syndicMainButton.frame.origin.x,
                                                                       (syndicMainButton.frame.origin.y+(syndicMainButton.frame.size.height+3)),
                                                                       syndicMainButton.frame.size.width, syndicMainButton.frame.size.height/2)];
    [syndicMainLabel setTextAlignment:NSTextAlignmentCenter];
    [syndicMainLabel setBackgroundColor:[UIColor clearColor]];
    [syndicMainLabel setTextColor:sharedAppDelegate.insideGreyColor];
    [syndicMainLabel setFont:sharedAppDelegate.insideMainFont];
    [syndicMainLabel setText:@"Syndicat"];
    
    [self.view addSubview:syndicMainLabel];
    [self.view addSubview:syndicMainButton];
    
    
    /////////////////////////////
    // Main Menu Loading
    /////////////////////////////
    
}

- (void) pushToView:(id)sender {
    
    UIButton *buttonClicked = sender;
    NSString *controllerName = buttonClicked.titleLabel.text;
    
    if(controllerName != nil){
    
        if([controllerName isEqualToString:@"Path"]){
            ItinerariesListViewController *toController = [[ItinerariesListViewController alloc] initWithDataType:nil];
            [self pushToNextView:toController];
        }else if ([controllerName isEqualToString:@"Event"]){
            EventListViewController *toController = [[EventListViewController alloc] initWithDataType:nil];
            [self pushToNextView:toController];
        }else if ([controllerName isEqualToString:@"Syndicat"]){
            [self presentInfoView];
        }else{
            PlacesListviewController *toController = [[PlacesListviewController alloc] initWithDataType:controllerName];
            [self pushToNextView:toController];
        }
        
    }
    
}


// Present InfoView

- (void) presentInfoView {
    [self presentCurrentView:self.infoViewController];
}



@end
