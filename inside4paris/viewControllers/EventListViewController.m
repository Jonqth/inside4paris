//
//  EventListViewController.m
//  inside4paris
//
//  Created by Jonathan Araujo-Levy on 19/12/12.
//  Copyright (c) 2012 hetic. All rights reserved.
//

#import "EventListViewController.h"

@interface EventListViewController ()

@end

@implementation EventListViewController

- (id)init {
    
    // Provenence
    self.isFromMenu = YES;
    
    self = [super init];
    if (self) {}
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self eventsUILoading];
	
    UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-75, 50, 150, 50)];
    [myLabel setText:@"Evénements"];
    [myLabel setBackgroundColor:[UIColor clearColor]];
    [myLabel setTextAlignment:NSTextAlignmentCenter];
    [myLabel setTextColor:sharedAppDelegate.insidePinkColor];
    [self.view addSubview:myLabel];
    
}

- (void) eventsUILoading {
    
    // Navigation button
    if(self.isFromMenu){ [self setMenuButton]; }
    else{ [self setBackButton]; }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
