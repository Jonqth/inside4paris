//
//  PlacesListviewController.m
//  inside4paris
//
//  Created by Jonathan Araujo-Levy on 18/12/12.
//  Copyright (c) 2012 hetic. All rights reserved.
//

#import "PlacesListviewController.h"
#import <QuartzCore/QuartzCore.h>

// View
#import "PlacesSingleViewController.h"

@interface PlacesListviewController (){
    UIButton *segmentedButton;
    BOOL isMap;
    int MainViewSizeHeight;
}

@end

@implementation PlacesListviewController

// Elements
@synthesize placesTableView;
@synthesize placesMapView;

// Location
@synthesize placesLocationManager;

- (id)init {
    
    // Provenence
    self.isFromMenu = YES;
    
    self = [super init];
    if (self) {}
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self placesLocationManagerInit];
    [self placesUILoading];
    
}

// Location Manager setting
- (void) placesLocationManagerInit {
    self.placesLocationManager = [[CLLocationManager alloc] init];
    [self.placesLocationManager setDelegate:self];
    [self.placesLocationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    [self.placesLocationManager setPausesLocationUpdatesAutomatically:YES];
    [self.placesLocationManager startUpdatingLocation];
    [self.placesLocationManager startUpdatingHeading];
}

// UILoading

- (void) placesUILoading {
    
    // Navigation button 
    if(self.isFromMenu){ [self setMenuButton]; }
    else{ [self setBackButton]; }
    
    // SegementedView
    isMap = NO;

    // MainViewSizeHeight
    MainViewSizeHeight = 372;
    if(sharedAppDelegate.mainScreenI5){
        MainViewSizeHeight += 88;
    }
    
    UIImageView *placeSegmentedView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 44, self.view.frame.size.width, 44)];
    [placeSegmentedView setImage:[UIImage imageNamed:@"i4p_segmentedControlBg.jpg"]];
    
    segmentedButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [segmentedButton setFrame:CGRectMake((self.view.frame.size.width/2)-101, placeSegmentedView.frame.size.height+8, 202, 27)];
    [segmentedButton setBackgroundImage:[UIImage imageNamed:@"i4p_segmentedNormal.png"] forState:UIControlStateNormal];
    [segmentedButton setBackgroundImage:[UIImage imageNamed:@"i4p_segmentedNormal.png"] forState:UIControlStateHighlighted];
    [segmentedButton addTarget:self action:@selector(didSegmentChangeState) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:placeSegmentedView];
    [self.view addSubview:segmentedButton];
    
    
    // TableView
    self.placesTableView = [[UITableView alloc] initWithFrame:CGRectMake(6, (placeSegmentedView.frame.origin.y+placeSegmentedView.frame.size.height),
                                                                         self.view.frame.size.width-12, MainViewSizeHeight)];
    [self.placesTableView setDelegate:self];
    [self.placesTableView setDataSource:self];
    [self.placesTableView setShowsVerticalScrollIndicator:NO];
    [self.placesTableView setAlwaysBounceVertical:NO];
    [self.placesTableView setBackgroundColor:[UIColor clearColor]];
    [self.placesTableView setSeparatorColor:[UIColor clearColor]];
    [self.placesTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:self.placesTableView];
    
    // MapView
    self.placesMapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, (placeSegmentedView.frame.origin.y+placeSegmentedView.frame.size.height),
                                                                     self.view.frame.size.width, MainViewSizeHeight)];
    [self.placesMapView setDelegate:self];
    [self.placesMapView setMapType:MKMapTypeStandard];
    [self.placesMapView setHidden:YES];
    [self.placesMapView setUserTrackingMode:MKUserTrackingModeFollow];

    [self.view addSubview:self.placesMapView];
}

- (void) didSegmentChangeState {
    if(isMap == NO) {
        [segmentedButton setBackgroundImage:[UIImage imageNamed:@"i4p_segmentedSelected.png"] forState:UIControlStateNormal];
        [self.placesTableView setHidden:YES];
        [self.placesMapView setHidden:NO];
        isMap = YES;
    }else {
        [segmentedButton setBackgroundImage:[UIImage imageNamed:@"i4p_segmentedNormal.png"] forState:UIControlStateNormal];
        [self.placesTableView setHidden:NO];
        [self.placesMapView setHidden:YES];
        isMap = NO;
    }
}


 /*
 ////////////////////////////////////////////////////////////////////////
 // TableView Methods
 */

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    PlacesSingleViewController *toController = [[PlacesSingleViewController alloc] init];
    [self pushToNextView:toController];

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 10)];
    [headerView setBackgroundColor:[UIColor clearColor]];
    
    return headerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 87;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Set the data for this cell
    UIView *singlePlaceCellView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.placesTableView.frame.size.width, indexPath.accessibilityFrame.size.height)];
    [singlePlaceCellView setBackgroundColor:[UIColor whiteColor]];
    singlePlaceCellView.layer.cornerRadius = 5;
    singlePlaceCellView.layer.masksToBounds = YES;
    singlePlaceCellView.layer.shadowColor = [UIColor blackColor].CGColor;
    singlePlaceCellView.layer.shadowOffset = CGSizeMake(0, 0.2);
    singlePlaceCellView.layer.shadowOpacity = 0.3;
    singlePlaceCellView.layer.shadowRadius = 0.4;
    singlePlaceCellView.clipsToBounds = NO;
    
    UIView *selectedView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.placesTableView.frame.size.width, indexPath.accessibilityFrame.size.height)];
    [selectedView setBackgroundColor:[UIColor whiteColor]];
    selectedView.layer.cornerRadius = 5;
    selectedView.layer.masksToBounds = YES;
    selectedView.layer.shadowColor = [UIColor blackColor].CGColor;
    selectedView.layer.shadowOffset = CGSizeMake(0, 0.2);
    selectedView.layer.shadowOpacity = 0.3;
    selectedView.layer.shadowRadius = 0.4;
    selectedView.clipsToBounds = NO;
    
    UIImageView *singlePlaceImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"i4p_genPlace.png"]];
    [singlePlaceCellView addSubview:singlePlaceImageView];
    
    // Icon Type
    UIImage *foodIconImage = [UIImage imageNamed:@"i4p_placeFoodIcon.jpg"];
    UIImageView *foodIconView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 50, foodIconImage.size.width, foodIconImage.size.height)];
    [foodIconView setImage:foodIconImage];
    [singlePlaceCellView addSubview:foodIconView];
    
    // Text Name
    UILabel *cellTitleText = [[UILabel alloc] initWithFrame:CGRectMake(45, 51, 150, 20)];
    [cellTitleText setFont:sharedAppDelegate.insideMainFont];
    [cellTitleText setText:@"Dans le Noir"];
    [singlePlaceCellView addSubview:cellTitleText];
    
    // Marker
    UIImage *markerImage = [UIImage imageNamed:@"i4p_placeMarker.png"];
    UIImageView *markerIconView = [[UIImageView alloc] initWithFrame:CGRectMake(45, 72, markerImage.size.width, markerImage.size.height)];
    [markerIconView setImage:markerImage];
    [singlePlaceCellView addSubview:markerIconView];
    
    // Text adress
    UILabel *cellTextAdress = [[UILabel alloc] initWithFrame:CGRectMake(57, 67, 150, 20)];
    [cellTextAdress setFont:sharedAppDelegate.insideTinyFont];
    [cellTextAdress setTextColor:sharedAppDelegate.insideGreyColor];
    [cellTextAdress setBackgroundColor:[UIColor clearColor]];
    [cellTextAdress setText:@"51, rue de Quincampoix"];
    [singlePlaceCellView addSubview:cellTextAdress];
    
    // Arrow
    UIImage *arrowImage = [UIImage imageNamed:@"i4p_arrowList.png"];
    UIImageView *arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(290, 65, arrowImage.size.width, arrowImage.size.height)];
    [arrowImageView setImage:arrowImage];
    [singlePlaceCellView addSubview:arrowImageView];
    
    
    
    // Subviews
    [cell setSelectedBackgroundView:selectedView];
    [cell setBackgroundView:singlePlaceCellView];

    return cell;
}

/*
 // TableView Methods
 ////////////////////////////////////////////////////////////////////////
 */



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
