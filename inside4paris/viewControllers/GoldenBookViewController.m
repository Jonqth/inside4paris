//
//  GoldenBookViewController.m
//  inside4paris
//
//  Created by Jonathan Araujo-Levy on 19/12/12.
//  Copyright (c) 2012 hetic. All rights reserved.
//

#import "GoldenBookViewController.h"

@interface GoldenBookViewController ()

@end

@implementation GoldenBookViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self goldenBookUILoading];
	
    UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-75, 50, 150, 50)];
    [myLabel setText:@"Livre D'or"];
    [myLabel setBackgroundColor:[UIColor clearColor]];
    [myLabel setTextAlignment:NSTextAlignmentCenter];
    [myLabel setTextColor:sharedAppDelegate.insidePinkColor];
    [self.view addSubview:myLabel];
    
}

- (void) goldenBookUILoading {
    [self setMenuButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
